
if (Posts.find().count() === 0) {
  Posts.insert({
    title: 'Introduccion a telescope',
    author: 'Sacha Greif',
    url: 'http://sachagreif.com/introducing-telescope/'
  });

  Posts.insert({
    title: 'Meteor',
    author: 'Tom Coleman',
    url: 'http://meteor.com'
  });

  Posts.insert({
    title: 'El libro de Meteor',
    author: 'Tom Coleman',
    url: 'http://themeteorbook.com'
  });
}
