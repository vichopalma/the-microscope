//checkea que ese userId en especifico sea dueño de el documento
ownsDocument = function(userId, doc) {
  return doc && doc.userId === userId;
}
