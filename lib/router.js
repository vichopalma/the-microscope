Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
  waitOn: function() { return Meteor.subscribe('posts'); }
});

//ruta para la pagina principal
Router.route('/', {name: 'postsList'});

//ruta para posts en especifico
Router.route('/posts/:_id', {
  name: 'postPage',
  //cada vez que un usuario accede a esta ruta, encontraremos el post adecuado y
  // lo pasaremos a la plantilla.
  data: function() { return Posts.findOne(this.params._id); }
});

//se agrega la ruta para editar post
Router.route('/posts/:_id/edit', {
  name: 'postEdit',
  data: function() { return Posts.findOne(this.params._id); }
});

//ruta para crear los post
Router.route('/submit', {name: 'postSubmit'});

var requireLogin = function() {
  if (! Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render('accessDenied');
    }
  } else {
    this.next();
  }
}
//ruta para cuando se devuelva un objeto falso (o null, false, undefined o vació).
Router.onBeforeAction('dataNotFound', {only: 'postPage'});
Router.onBeforeAction(requireLogin, {only: 'postSubmit'});
