Template.postItem.helpers({
  //muestra enlace siempre y cuando sea el usuario creador del post
  //al "dueño" del post
  ownPost: function(){
    return this.userId === Meteor.userId();
  },

  domain: function() {
    var a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  }
});
