Template.postEdit.events({
  'submit form': function(e) {
    e.preventDefault();

    var currentPostId = this._id;

    var postProperties = {
      url: $(e.target).find('[name=url]').val(),
      title: $(e.target).find('[name=title]').val()
    }

    //creacion del evento (callback) update (actualizar)
    Posts.update(currentPostId, {$set: postProperties}, function(error) {
      if (error) {
        // muestra el error al usuario
        alert(error.reason);
      } else {
        Router.go('postPage', {_id: currentPostId});
      }
    });

  },

//creacion del evento (callback) "eliminar post" con trigger onClick en boton delete
  'click .delete': function(e) {
    //obtiene el post
    e.preventDefault();

    //con una confimacion rpegunta si de verdad se quiere eliminar el post
    if (confirm("Delete this post?")) {
      //de ser verdad, se crea una variable y se le asigna la id del post actual
      var currentPostId = this._id;
      //luego se procede a eliminar el post con el metodo de las colleciones "remove"
      Posts.remove(currentPostId);
      //finalmente se re-dirige al usuario a la lista de posts
      Router.go('postsList');
    }
  }
});
