/*Ahora que tenemos una fecha de envío
en todos nuestros posts, tiene sentido
asegurarnos que se están ordenando
usando este atributo. Para ello
usaremos el operador sort de Mongo
que espera un objeto que consta de las
 claves de ordenación, y un
 signo que indica si son ascendentes o
 descendentes
*/

Template.postsList.helpers({
  posts: function() {
    return Posts.find({}, {sort: {submitted: -1}});
  }
});
